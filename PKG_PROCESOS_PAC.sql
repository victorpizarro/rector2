CREATE OR REPLACE PACKAGE PKG_PROCESOS_PAC IS
-- MODIFICACIONES --
--20110310-SGALLEGOS-[ER-RECTOR-PROYCOB_EST_REG]--SE MODIFICAN Y SE CREAN FUNCIONES PARA PERU...
--20110311-SGALLEGOS-[ER-RECTOR-PROYCOB_EST_REG] :AGREGAR_CUOTA=> SE ASIGNAN NUMERO DE RECIBO Y ENDOSO RECIBIDO POR PARAMETRO
--TRASPASO_ENDOSO_COBRANZA=>SE AGREGA CREACION DE RECIBOS..+ EXCEPCION
--20110303-SE CREA FUNCION DE ACTUALIZACION DE FECHAS DE RECIBO =>FN_ACTUALIZA_FECHAS_RECIBO
--20110303-SE CREA FUNCION DE ACTUALIZACION ESTADO RECIBO  =>FN_ACTUALIZA_ESTADO_RECIBO
--20110304-SE CREA FUNCION PARA HOMOLOGAR ESTADO DE CTA CTE CON RECIBO=>FN_HOMOLOGA_ST_CCTE_RECIBOS
--20110304-SE CREA FUNCION PARA SABER SI HAY UN PAC ACTIVO PARA UN ID DE CTA CTE  =>FN_EXISTE_PAC_ACTIVO
--20110304-SE CREA FUNCION QUE DEVUELVE TODAS LAS CUOTAS PENDIENTES SEGUN PROPUESTA=>PROC_RETORNA_CUOTAS_PENDIENTES
--20110304-SE CREA FUNCION PARA ACTUALIZAR CAMPOS PARA UN ID DE CTA CTE =>FN_ACTUALIZA_CAMPOS_CTE_CTE  --
--20110304-SE CREA FUNCION PARA ACTUALIZA LA PRIMA BRUTA SEGUN PROPUESTA  =>FN_ACT_PRIMA_BRUTA
--20110304-SE CREA FUNCION QUE RETORNA TODAS LAS CUOTAS ANULADAS (ESTADO1)
--20110307-SE CREA FUNCION PARA ACTUALIZA CUADRATURA GUARDANDO ESTADO ANTERIOR =>ACTUALIZA_ST_CUADRATURA_DIARIA  -
--20110307-SE CREA FUNCION PARA GRABAR CARGOS DESDE RECTOR-ESTA SE MIGRA DESDE CHILE (PKG:PKG_SOLICTUD_ANULACION.GRABAR_NORMALIZACION)
--20110310-SE MODIFICA FUNCION MIGRADA A PERU.CAMBIA_ESTADO_CUOTA A =>CAMBIA_DATOS_CUOTA_PAC , AGREGANDO PARAMETROS FALTANTES (RECIBO , ENDOSO)
--20110310-SE CREA FUNCION DE REACTIVACION PARA ACTUALIZAR LA SCO_CUADRATURA_DIARIA (DE 3 CAMBIA A ESATDO ANTERIOR),
--20110311-SE CREA FUNCION QUE ACTUALIZA INSCRIPCION PARA ENDOSO DE CAMBIO DE CUENTA=>FN_ENDOSO_CAMBIO_CTA_PAC
--20110331-SGALLEGOS-EN TRASPASO_ENDOSO_COBRANZA=>SE AGREGA CONDICION PARA GENERAR RECIBOS
--20120104-LMORA-SE MODIFICAN FUNCIONES FN_AGREGAR_CUOTA_PAC Y GRABAR_NORMALIZACION --
--20120725-SHGALLEGOS- V.1.19 SE COMENTA FUNCION ANTIGUA FN_AGREGAR_CUOTA_PAC
--20140310-YGP-SE MODIFICA FN_CALCULA_DIA_PAC RESPECTO A VALIDACION DE CONFIGURACION DE MIGRACION DE PRODUCTO
--20151229--CFSM-SE AGREGAN FUNCIONES PARA CAMBIO DE ARCHIVOS PAC DESA-1550

--SE AGREGO 14-04-20156
-- ESTADOS DE LOS RECIBOS DE RECTOR CART_RECIBOS
CARE_ST_RECIBO_PENDIENTE       CONSTANT NUMBER := 1;
CARE_ST_RECIBO_REZAGADO        CONSTANT NUMBER := 2;
CARE_ST_RECIBO_ANULADO         CONSTANT NUMBER := 3;
CARE_ST_RECIBO_COBRADO         CONSTANT NUMBER := 4;
CARE_ST_RECIBO_DEVUELTO        CONSTANT NUMBER := 5;
CARE_ST_RECIBO_COBRO_DIF       CONSTANT NUMBER := 6;
CARE_ST_RECIBO_CUOTA_GRATIS    CONSTANT NUMBER := 7;
CARE_ST_RECIBO_NO_REALIZADO    CONSTANT NUMBER := 8;
CARE_ST_RECIBO_SIN_MED_PAGO    CONSTANT NUMBER := 9;

--------------------------------------------------------------------------
-- STATUS DE OPERACION, TABLA SCO_OP_DIARIAS --
CDOP_ST_PENDIENTE             CONSTANT NUMBER(2) := 0;
CDOP_ST_TRANSF_CUADR_DIARIA   CONSTANT NUMBER(2) := 1;
--------------------------------------------------------------------------
-- MOVIMIENTO, TABLA SCO_CUENTA_CORRIENTE --
CCTE_TRASP_ENV_CARGOS_SI       CONSTANT VARCHAR2(1) := 'S';
CCTE_TRASP_ENV_CARGOS_NO       CONSTANT VARCHAR2(1) := 'N';
---------------------------------------------------------------------------
-- MOVIMIENTO, TABLA SCO_CUENTA_CORRIENTE --
CCTE_MOVTO_DEBE                CONSTANT VARCHAR2(1) := 'D';
CCTE_MOVTO_HABER               CONSTANT VARCHAR2(1) := 'H';
---------------------------------------------------------------------------
-- TIPO DE MOVIMIENTO, TABLA SCO_CUENTA_CORRIENTE --
CCTE_TP_MOVTO_CARGO_NOR       CONSTANT NUMBER(2) := 1;
CCTE_TP_MOVTO_CARGO_DIR       CONSTANT NUMBER(2) := 2;
CCTE_TP_MOVTO_ABONO_DIR       CONSTANT NUMBER(2) := 3;
CCTE_TP_MOVTO_CARGO_DIR_SIN   CONSTANT NUMBER(2) := 4;
CCTE_TP_MOVTO_ABONO_DIR_SIN   CONSTANT NUMBER(2) := 5;
CCTE_TP_MOVTO_ABONO_DIR_CAJA  CONSTANT NUMBER(2) := 6;
CCTE_TP_MOVTO_ABONO_DIR_CHEQ  CONSTANT NUMBER(2) := 7;
CCTE_TP_MOVTO_ABONO_DIR_INT   CONSTANT NUMBER(2) := 8;
---------------------------------------------------------------------------
-- STATUS DE CUENTA CORRIENTE, TABLA SCO_CUENTA_CORRIENTE --
CCTE_ST_PENDIENTE             CONSTANT NUMBER(2) := 0;
CCTE_ST_ANULADO               CONSTANT NUMBER(2) := 1;
CCTE_ST_REZAGADO              CONSTANT NUMBER(2) := 4;
CCTE_ST_ACEPTADO              CONSTANT NUMBER(2) := 5;
CCTE_ST_POR_REVISAR                  CONSTANT NUMBER(2) := 6;
CCTE_ST_CUOTA_GRATIS          CONSTANT NUMBER(2) := 11;
CCTE_ST_NO_REALIZADO          CONSTANT NUMBER(2) := 15;
CCTE_ST_CARGO_CAJA_CMR        CONSTANT NUMBER(2) := 30;
CCTE_ST_SIN_MEDIO_PAGO        CONSTANT NUMBER(2) := 69;
--------------------------------------------------------------------------------------------------------------------------
-- CODIGO DE ALTAS Y BAJAS CMR --
CODIGO_ALTA_INSC_CMR           CONSTANT VARCHAR2(1) := 'A';
CODIGO_BAJA_INSC_CMR           CONSTANT VARCHAR2(1) := 'B';
--------------------------------------------------------------------------------------------------------------------------
-- STATUS DE CUADRATURA, TABLA SCO_CUADRATURA_DIARIA --
SCCD_ST_CERT_ACEP              CONSTANT NUMBER := 0;
SCCD_ST_CERT_RECH              CONSTANT NUMBER := 1;
SCCD_ST_INSC_RECH              CONSTANT NUMBER := 2;
SCCD_ST_INSC_ACEP_SIN_CTACTE   CONSTANT NUMBER := 3;
SCCD_ST_ERR_NO_EXIST_PPTA      CONSTANT NUMBER := 4;
SCCD_ST_SIN_ANULACION_CURSADA  CONSTANT NUMBER := 5;
SCCD_ST_CON_ANULACION_CURSADA  CONSTANT NUMBER := 6;
SCCD_ST_ERR_NO_EXIST_PRIM_MON  CONSTANT NUMBER := 7;
SCCD_ST_INICIAL_TRANSFERENCIA  CONSTANT NUMBER := 8;
SCCD_ST_ERR_NO_EXIST_PRODPAC   CONSTANT NUMBER := 9;
SCCD_ST_PRODUCTO_NO_PAC        CONSTANT NUMBER := 10;
SCCD_ST_ERR_GEN_INSC_PROD_PAC  CONSTANT NUMBER := 11;
SCCD_ST_ERR_GEN_INSC_DAT_CLI   CONSTANT NUMBER := 12;
SCCD_ST_ERR_GEN_INSC_PROD_REC  CONSTANT NUMBER := 13;
SCCD_ST_ERR_GEN_INSC_CTA_CMR   CONSTANT NUMBER := 14;
SCCD_ST_ERR_GEN_INSC_INS_REG   CONSTANT NUMBER := 15;
SCCD_ST_INSC_ACEP_CON_CTACTE   CONSTANT NUMBER := 16;
SCCD_ST_INSC_CAMBIO_CTA_CMR    CONSTANT NUMBER := 16;--EN LA CUADRATURA DEBE QUEDAR CON ESTADO 16
SCCD_ST_INSC_CAMBIO_CUENTA     CONSTANT NUMBER := 17;
SCCD_ST_INSC_PENDIENTE_ENV     CONSTANT NUMBER := 18;
SCCD_ST_INSC_RECEPCIONADA      CONSTANT NUMBER := 19;
SCCD_ST_ERR_GEN_INSC_SIN_CCTE  CONSTANT NUMBER := 20;
SCCD_ST_ERR_GEN_INSC_ID_BANCO  CONSTANT NUMBER := 21;
SCCD_ST_INSC_EN_PROCESO_RESP   CONSTANT NUMBER := 22;
SCCD_ST_CERT_PEND              CONSTANT NUMBER := 23;
SCCD_ST_VENCIDA_RECTOR         CONSTANT NUMBER := 24;
SCCD_ST_SUSPENDIDA_RECTOR      CONSTANT NUMBER := 25;
SCCD_ST_NO_RENOVADA_RECTOR     CONSTANT NUMBER := 26;
SCCD_ST_INSC_SIN_RESP_SIN_CCTE CONSTANT NUMBER := 27;
SCCD_ST_INSC_SIN_RESP_CON_CCTE CONSTANT NUMBER := 28;
SCCD_ST_INSC_CAMBIO_MEDIO_PAGO CONSTANT NUMBER := 29;
SCCD_ST_INSC_DESEST_CARGOS     CONSTANT NUMBER := 30;
--SCCD_ST_INSC_DESIST_CARGOS     CONSTANT NUMBER := 30;
SCCD_ST_ERR_GEN_INSC_BOL_LIP   CONSTANT NUMBER := 31;
SCCD_ST_ERR_GEN_INSC_DFAC_LIP  CONSTANT NUMBER := 32; -- DIA DE FACTURACION --
SCCD_ST_ERR_GEN_INSC_CTA_BCO   CONSTANT NUMBER := 33;
SCCD_ST_ERR_CSBF               CONSTANT NUMBER := 34;
SCCD_ST_ERR_GEN_CTA_CTE_RECIBO CONSTANT NUMBER := 35;
SCCD_ST_ERR_PTA_NULA_EN_RECTOR CONSTANT NUMBER := 36;
SCCD_ST_ERR_FRAGMENT_RECIBOS   CONSTANT NUMBER := 37;
SCCD_ST_ERR_RECIBO_OP_DIARIA   CONSTANT NUMBER := 38;
SCCD_ST_ERR_FECHA_PAC_CERT     CONSTANT NUMBER := 39;
SCCD_ST_ERR_PROPUESTA_NO_VIG   CONSTANT NUMBER := 40;
---------------------------------------------------------------------------
-- STATUS DE INSCRIPCION, TABLA SCO_INSCRIPCION --
SCOI_ST_PENDIENTE_ENVIO        CONSTANT NUMBER := 0; --ESTADO 1 PARA TODOS LOS PAISES EXCEPTO CHILE POR EL PROCESO DE INSCRIPCION
SCOI_ST_INSC_ACEP              CONSTANT NUMBER := 1;
SCOI_ST_INSC_RECH              CONSTANT NUMBER := 2;
SCOI_ST_CAMBIO_CUENTA          CONSTANT NUMBER := 4;
--SCOI_ST_CAMBIO_CUENTA_CMR      CONSTANT NUMBER := 4; --ESTADO 1 PARA TODOS LOS PAISES EXCEPTO CHILE POR EL PROCESO DE INSCRIPCION
SCOI_ST_INSC_ACEP_DIA_PAG_CERO CONSTANT NUMBER := 5;
SCOI_ST_CAMBIO_MEDIO_PAGO      CONSTANT NUMBER := 6;
SCOI_ST_DESEST_CARGOS          CONSTANT NUMBER := 7;
SCOI_ST_INICIAL_SIN            CONSTANT NUMBER := 10;
SCOI_ST_INSC_ACEP_SIN          CONSTANT NUMBER := 11;
SCOI_ST_INSC_RECH_SIN          CONSTANT NUMBER := 12;

---------------------------------------------------------------------------
-- TIPO DE REGISTRO EN BITACORA DE CAMBIO DE CUENTA O MEDIO DE PAGO --
SCHCMP_TP_REGISTRO_PAGO        CONSTANT VARCHAR2(1) := 'P';
SCHCMP_TP_REGISTRO_CMR         CONSTANT VARCHAR2(1) := 'C';
---------------------------------------------------------------------------
-- MOTIVOS DE ENDOSOS EN TABLA SCOH_INSCRIPCION --
CAME_TP_TRANSAC_END_CUALIT     CONSTANT VARCHAR2(1) := 'L';
CAME_TP_TRANSAC_ANULACION      CONSTANT VARCHAR2(1) := 'A';
CAME_CD_MOTIVO_CAMB_CUENTA_CMR CONSTANT NUMBER      := 4;
CAME_CD_MOTIVO_CAMB_MED_PAGO   CONSTANT NUMBER      := 520;
---------------------------------------------------------------------------
-- TIPO DE INSCRIPCION, TABLA SCO_INSCRIPCION --
SCOI_TP_INSC_GENERAL           CONSTANT NUMBER := 0;
SCOI_TP_INSC_NO_MASIVOS        CONSTANT NUMBER := 1;
SCOI_TP_INSC_MASIVOS           CONSTANT NUMBER := 2;
---------------------------------------------------------------------------
-- TIPO DE ARCHIVO, TABLA SCO_PLANO_ENVIO_CARGOS --
TP_ARCH_GENERAL                CONSTANT NUMBER      := 0;
--TP_ARCH_NO_MASIVOS             CONSTANT NUMBER      := 1;
--TP_ARCH_MASIVOS                CONSTANT NUMBER      := 2;
--TP_ARCH_RENDICION              CONSTANT NUMBER      := 3;
--TP_ARCH_SIGLOXXI               CONSTANT NUMBER      := 4;
--TP_ARCH_HONDA                  CONSTANT NUMBER      := 5;
---------------------------------------------------------------------------
-- TIPO DE ARCHIVO, TABLA SCO_PLANO_ENVIO_CARGOS --
SCOI_TP_ARCH_GENERAL           CONSTANT NUMBER := 0;
SCOI_TP_ARCH_NO_MASIVOS        CONSTANT NUMBER := 1;
SCOI_TP_ARCH_MASIVOS           CONSTANT NUMBER := 2;
---------------------------------------------------------------------------
CODIGO_OPERACION_TRASP_CARTERA CONSTANT NUMBER(4) := 9999;
CODIGO_OPERACION_VENTA_NUEVA   CONSTANT NUMBER(4) := 1001;
CODIGO_OPERACION_ANULACION     CONSTANT NUMBER(4) := 1003;
---------------------------------------------------------------------------
CODIGO_MEDIO_PAGO_SIN_MED_PAG  CONSTANT NUMBER := 0;
CODIGO_MEDIO_PAGO_SIC          CONSTANT NUMBER := 1;
CODIGO_MEDIO_PAGO_CMR          CONSTANT NUMBER := 5;
CODIGO_MEDIO_PAGO_VISANET      CONSTANT NUMBER := 8;
--CODIGO_MEDIO_PAGO_MAND_BANC    CONSTANT NUMBER := 9;
---------------------------------------------------------------------------
-- CODIGO DE PAISES --
CODIGO_PAIS_CHILE              CONSTANT NUMBER      := 1;
CODIGO_PAIS_PERU               CONSTANT NUMBER      := 2;
CODIGO_PAIS_ARGENTINA          CONSTANT NUMBER      := 3;
CODIGO_PAIS_COLOMBIA           CONSTANT NUMBER      := 4;
---------------------------------------------------------------------------
-- CODIGOS DE MONEDAS --
CODIGO_MONEDA_DOLAR            CONSTANT VARCHAR2(2) := '02';
-- NO UTILIZADO EN PERU --
--CODIGO_MONEDA_UF               CONSTANT VARCHAR2(2) := '02';
CODIGO_MONEDA_SOLES            CONSTANT VARCHAR2(2) := '01';
CODIGO_MONEDA_PESOS            CONSTANT VARCHAR2(2) := '01'; -- POR SI ES LLAMADO DE UN FORMS --
---------------------------------------------------------------------------
SCCMP_ID_SIC_CMR               CONSTANT NUMBER := 1;
SCCMP_ID_CMR_SIC               CONSTANT NUMBER := 2;
SCCMP_ID_MANDATO_BANC_CMR      CONSTANT NUMBER := 3;
SCCMP_ID_MANDATO_BANC_SIC      CONSTANT NUMBER := 4;
---------------------------------------------------------------------------
SCDAI_ID_PROCESO_ENV           CONSTANT VARCHAR2(1) := 'E';
SCDAI_ID_PROCESO_RESP          CONSTANT VARCHAR2(1) := 'R';
SCDAI_ID_PROCESO_ERR           CONSTANT VARCHAR2(1) := 'X';
---------------------------------------------------------------------------
SCDAC_ID_PROCESO_ENV           CONSTANT VARCHAR2(1) := 'E';
SCDAC_ID_PROCESO_RESP          CONSTANT VARCHAR2(1) := 'R';
SCDAC_ID_PROCESO_ERR           CONSTANT VARCHAR2(1) := 'X';
---------------------------------------------------------------------------
SCDAC_ID_PROCESO_ENV           CONSTANT VARCHAR2(1) := 'E';
SCDAC_ID_PROCESO_RESP          CONSTANT VARCHAR2(1) := 'R';
SCDAC_ID_PROCESO_ACEP          CONSTANT VARCHAR2(1) := 'A';
SCDAC_ID_PROCESO_RECH          CONSTANT VARCHAR2(1) := 'N'; -- NO ACEPTADDOS -- RECHAZADOS --
SCDAC_ID_PROCESO_ERR           CONSTANT VARCHAR2(1) := 'X';
------------------------------------------------------------------------------------------------------------------------------------------------
RESP_ARCH_PAC_SI               CONSTANT VARCHAR2(1) := 'S';
RESP_ARCH_PAC_NO               CONSTANT VARCHAR2(1) := 'N';
---------------------------------------------------------------------------
-- CAMPO DE CONTROL DE REGISTROS ENVIADOS Y RESPONDIDOS EN LOS PROCESOS DE COBRANZA --
CCTE_CAMPO7_PEND_ENV           CONSTANT NUMBER(2)   := 0;
CCTE_CAMPO7_RESP               CONSTANT NUMBER(2)   := 1;
CCTE_CAMPO7_ENV                CONSTANT NUMBER(2)   := 2;
--------------------------------------------------------------------------------------------------------------------------
RESP_ARCH_PAC_SI               CONSTANT VARCHAR2(1) := 'S';
RESP_ARCH_PAC_NO               CONSTANT VARCHAR2(1) := 'N';
------------------------------------------------------------------------------------------------------------------------------------------------
CCTE_MOVTO_CONTABLE_SI         CONSTANT VARCHAR2(1) := 'S';
CCTE_MOVTO_CONTABLE_NO         CONSTANT VARCHAR2(1) := 'N';
------------------------------------------------------------------------------------------------------------------------------------------------
SCDAC_ID_PROCESO_ENV           CONSTANT VARCHAR2(1) := 'E';
SCDAC_ID_PROCESO_RESP          CONSTANT VARCHAR2(1) := 'R';
SCDAC_ID_PROCESO_ACEP          CONSTANT VARCHAR2(1) := 'A';
SCDAC_ID_PROCESO_RECH          CONSTANT VARCHAR2(1) := 'N'; -- NO ACEPTADDOS -- RECHAZADOS --
SCDAC_ID_PROCESO_ERR           CONSTANT VARCHAR2(1) := 'X';
-------------------------------------------------------------------------------------------------------------------------------------
-- CAME_CD_MOTIVO_CAMB_MED_PAGO   CONSTANT NUMBER      := 520;
--------------------------------------------------------------------------------------------------------------------------
-- SCHCMP_TP_REGISTRO_CMR         CONSTANT VARCHAR2(1) := 'C';
--------------------------------------------------------------------------------------------------------------------------
-- MMA, 25-04-2012 
SCDMI_CD_MODO_INSC_IMPLICITA   CONSTANT NUMBER := 1;
--------------------------------------------------------------------------------------------------------------------------
-- MMA, 27-06-2012
TIPO_DESFASE_DIA               CONSTANT NUMBER := 1;
TIPO_DESFASE_MES               CONSTANT NUMBER := 2;
--------------------------------------------------------------------------------------------------------------------------
TYPE TDATOSSTATUS IS REF CURSOR;
TYPE TCURSOR      IS REF CURSOR;
TYPE CCURSORREG   IS REF CURSOR;
---------------------------------------------------------------------------
TYPE TSTATUS IS RECORD
(
CIERRE          NUMBER,
MEDIO_PAGO      NUMBER,
DESC_MEDIO_PAGO VARCHAR2(100),
ESTADO          NUMBER,
DESC_ESTADO     VARCHAR2(100),
CANTIDAD        NUMBER
);
---------------------------------------------------------------------------
TYPE SDUPLICADOS IS RECORD(
  PROPUESTA NUMBER(15),
  CUOTA     NUMBER(2),
  CANTIDAD  NUMBER(10)
  );
---------------------------------------------------------------------------
TYPE SEXCEL1 IS RECORD(
  NOMBRE_FANTASIA    VARCHAR2(32),
  TOTAL              NUMBER(15),
  VALOR              NUMBER(15)
  );
---------------------------------------------------------------------------
TYPE SEXCEL2 IS RECORD(
  COD_RESPUESTA_CMR  NUMBER(10),
  NOMBRE_FANTASIA    VARCHAR2(32),
  TOTAL              NUMBER(15),
  VALOR              NUMBER(15)
  );

-- ext_cenrperez, 24-09-2015, proyecto peru
PROCEDURE BUSCAR_EN_MIRROR(CUENTA_CMR IN VARCHAR2, S_MENSAJE OUT VARCHAR2);

---------------------------------------------------------------------------
-- LUIS MORA DIAZ --
-- 08/05/2011 --
-- FUNCION DE CALCULO DE TASA DE CAMBIO PARA COBRANZA PAC REGIONAL --
FUNCTION FN_CALCULO_TASA_CAMBIO_PAC (
                                     PE_FECHA_CAMBIO           IN     DATE, -- FECHA DE CAMBIO --
                                     PE_CD_MONEDA              IN     CART_MONEDAS.CAMO_CD_MONEDA%TYPE, -- MONEDA DE CAMBIO --
                                     PE_MONTO_MONEDA_CAMBIO    IN     NUMBER, -- MONTO EN MONEDA DE CAMBIO --
                                     PS_MONTO_MON_LOCAL_CAMBIO IN OUT NUMBER, -- MONTO DEL CAMBIO --
                                     PS_TASA_CAMBIO            IN OUT NUMBER, -- VALOR TASA DE CAMBIO APLICADA --
                                     PS_MENSAJE_ERROR          IN OUT VARCHAR2 -- MENSAJE DE ERROR --
                                     ) RETURN BOOLEAN;
---------------------------------------------------------------------------
-- LUIS MORA DIAZ --
-- 08/05/2011 --
-- SE AGREGA FUNCION PARA OBTENER Y CONTROLAR MONEDA LOCAL POR PAIS --
FUNCTION FN_OBTENER_MONEDA_LOCAL_PAIS (
                                       PS_CD_MONEDA     IN OUT CART_MONEDAS.CAMO_CD_MONEDA%TYPE, -- MONEDA LOCAL DEL PAIS --
                                       PS_MENSAJE_ERROR IN OUT VARCHAR2 -- MENSAJE DE ERROR --
                                       ) RETURN BOOLEAN;
---------------------------------------------------------------------------
-- LUIS MORA DIAZ --
-- 08/05/2011 --
-- SE AGREGA FUNCION PARA OBTENER Y CONTROLAR CODIGO DEL PAIS --
FUNCTION FN_OBTENER_CODIGO_PAIS (
                                 PS_CD_PAIS       IN OUT CART_PAISES.CAPA_CD_PAIS%TYPE, -- CODIGO DEL PAIS --
                                 PS_MENSAJE_ERROR IN OUT VARCHAR2 -- MENSAJE DE ERROR --
                                 ) RETURN BOOLEAN;
---------------------------------------------------------------------------

FUNCTION FN_AGREGAR_CUOTA_PAC (
                               PE_PROPUESTA         IN      NUMBER,   --NRO PROPUESTA 
                               PE_CUOTA             IN      NUMBER,   --NRO CUOTA  
                               PE_PERIODO           IN      NUMBER,   --NRO PERIODO   
                               --PE_CCTE_STATUS       IN      NUMBER;   --ESTADO DE LA CUOTA -- PENDIENTE -- 
                               PE_FECHA_ENVIO_PAC   IN      DATE,     --FECHA ENVIO PAC--
                               PE_FECHA_DESDE       IN      DATE,     --FECHA INICIO VIGENCIA CUOTA  
                               PE_FECHA_HASTA       IN      DATE,     --FECHA FIN VIGENCIA CUOTA    
                               PE_TOTAL_CUOTAS      IN      NUMBER,   --NRO FRAGMENT 
                               PE_MONEDA            IN      VARCHAR2, --TIPO MONEDA 
                               PE_MONTO_MONEDA      IN      NUMBER,   --MONTO MONEDA  
                               PE_CCTE_PRIMA_AFECTA IN      NUMBER,   --MONTO PRIMA AFECTA   
                               PE_CCTE_PRIMA_EXENTA IN      NUMBER,   --MONTO PRIMA EXENTA   
                               PE_CCTE_IVA          IN      NUMBER,   --MONTO IVA 
                               PE_CCTE_RECIBO       IN      NUMBER ,  --NRO RECIBO RECTOR 
                               PE_CCTE_NU_ENDOSO    IN      NUMBER,   --NRO ENDOSO RECTOR 
                               PE_CAOP_CD_OPERACION IN      NUMBER,   --CODIGO OPERACION DIARIA --
                               PE_CAOP_CD_SUB_OPER  IN      NUMBER,   --CODIGO SUB OPERACION DIARIA --
                               PE_CD_USUARIO        IN      VARCHAR2, --CODIGO DEL USUARIO  
                               PS_MENSAJE_ERROR     IN OUT  VARCHAR2  --MENSAJE DE ERROR 
                               )RETURN BOOLEAN;
                               
---------------------------------------------------------------------                               
FUNCTION ULTIMA_CUOTA_PAC (
                           PE_PROPUESTA      IN     NUMBER,
                           PS_PERIODO        IN OUT NUMBER,
                           PS_CUOTA          IN OUT NUMBER,
                           PS_MENSAJE_ERROR  IN OUT VARCHAR2
                           ) RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
FUNCTION OBTENER_ID_REGISTRO_CTA_CTE RETURN NUMBER;
-----------------------------------------------------------------------------------------
FUNCTION OBTENER_USUARIO RETURN VARCHAR2;
-----------------------------------------------------------------------------------------
FUNCTION OBTENER_TERMINAL RETURN VARCHAR2;
-----------------------------------------------------------------------------------------
FUNCTION GRABAR_CUENTA_CORRIENTE (
                                  PE_REG_CTA_CTE   IN SCO_CUENTA_CORRIENTE%ROWTYPE,
                                  PS_MENSAJE_ERROR IN OUT VARCHAR2
                                  ) RETURN BOOLEAN;
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
FUNCTION EXISTE_CAMBIO_CUENTA (
                               PE_SCOIH_ID      IN     NUMBER,
                               PS_SCOIH_SCOI_ID IN OUT NUMBER
                               ) RETURN NUMBER;
-----------------------------------------------------------------------------------------
FUNCTION GENERA_PROCESO_PAC (
                             PE_CD_MEDIO_PAGO     IN     NUMBER,
                             PE_FECHA_PROCESO_PAC IN     DATE,
                             PE_ID_USUARIO        IN     VARCHAR2,
                             PE_ID_TERMINAL       IN     VARCHAR2,
                             PS_MENSAJE_ERROR     IN OUT VARCHAR2
                             ) RETURN BOOLEAN;
-------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION DESESTIMIENTO_DE_CARGOS (
                                  PE_NU_PROPUESTA  IN     NUMBER,
                                  PS_MENSAJE_ERROR IN OUT VARCHAR2
                                  ) RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION OBTENER_ID_SCOH_INSCRIPCION RETURN NUMBER;
------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION OBTENER_NOMBRE_ARCH_CARGOS (
                                     PE_CD_MEDIO_PAGO     IN     NUMBER,
                                     PE_TP_ARCHIVO        IN     NUMBER,
                                     PE_ID_PROCESO        IN     VARCHAR2,
                                     PE_FECHA_ARCH_CARGOS IN     DATE,
                                     PS_NOMBRE_ARCHIVO    IN OUT VARCHAR2,
                                     PS_MENSAJE_ERROR     IN OUT VARCHAR2
                                     ) RETURN BOOLEAN;

------------------------------------------------------------------------------------------------
-- SCV  24-05-2011
-- ESTA FUNCION YA NO OPERA --
-- LA REHBILITACI�N PAC SE ACOPL� AL ENDOSO DE RECTOR. --
/*
-- AGREGA CRISTIAN ROBLES PINO  14-04-2009
-- PARA PROCESO DE REHABILITACION DE PROPUESTAS
FUNCTION REHABILITAR_CTACTE (
                             PE_PROPUESTA     IN     NUMBER,
                             PS_MENSAJE_ERROR IN OUT VARCHAR2
                             ) RETURN BOOLEAN;
*/
------------------------------------------------------------------------------------------------
PROCEDURE ACTUALIZAR_CUADRATURA (
                                 PE_PROPUESTA    IN NUMBER,
                                 PE_NUEVO_ESTADO IN NUMBER
                                 );
------------------------------------------------------------------------------------------------
FUNCTION CALCULAR_DIA_PAC (
                           PE_PROPUESTA     IN     NUMBER,
                           PS_FEC_PAC       IN OUT DATE,
                           PS_MENSAJE_ERROR IN OUT VARCHAR2
                           ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION ELIMINA_ID_ENVIO_CARGOS (
                                  PE_ID        IN     NUMBER,
                                  PE_FECHA_PAC IN     DATE,
                                  PS_TXT_ERR   IN OUT VARCHAR2
                                  ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION MODIFICAR_CUENTA_CORRIENTE (
                                     PE_REG_CTA_CTE   IN     SCO_CUENTA_CORRIENTE%ROWTYPE,
                                     PS_MENSAJE_ERROR IN OUT VARCHAR2
                                     ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- CRP 2009/04/14 10:18
FUNCTION FN_CTACTE_CONDATA(P_NPROPUESTA NUMBER) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- SCV  23-04-2011 --  SE COPIA FUNCION DE PKG_CUENTA_CORRIENTE
FUNCTION FN_CCTE_CONTROL_ACCESO(
                                PE_USUARIO    IN SCO_USUARIOS.ID_USUARIO%TYPE,
                                PE_APLICACION IN SCO_USUARIOS.ID_APLICACION%TYPE
                                ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION OBTENER_CUOTA_PERIODO (
                                PE_PROPUESTA      IN     NUMBER,
                                PS_ULTIMA_CUOTA   IN OUT NUMBER,
                                PS_ULTIMO_PERIODO IN OUT NUMBER,
                                PS_MENSAJE_ERROR  IN OUT NUMBER
                                ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- LUIS MORA DIAZ --
-- 17/05/2011 --
-- FUNCION GRABAR ENDOSOS DE COBRANZA --
-- SE ELIMINA FUNCION PARA EL NUEVO MODELO DE COBRANZA PAC REGIONAL --
/*
FUNCTION GRABAR_ENDOSO_COBRANZA (
                                 PE_REG_SCO_END_COB IN     SCO_ENDOSO_COBRANZA%ROWTYPE,
                                 PS_MENSAJE_ERROR   IN OUT VARCHAR2
                                 ) RETURN BOOLEAN;
*/
------------------------------------------------------------------------------------------------
-- LUIS MORA DIAZ --
-- 08/05/2011 --
-- FUNCION DE TRASPASO DE ENDOSOS DE COBRANZA POR ID DESDE SCO_ENDOSO_COBRANZA A SCO_CUENTA_CORRIENTE --
-- SE ELIMINA FUNCION PARA EL NUEVO MODELO DE COBRANZA PAC REGIONAL --
/*
FUNCTION TRASPASO_ENDOSO_COBRANZA_ID (
                                  PE_CCTE_ID           IN NUMBER,
                                  PS_MENSAJE_ERROR IN OUT VARCHAR2
                                  ) RETURN BOOLEAN;
*/
------------------------------------------------------------------------------------------------
FUNCTION SW_PROCESOS_PAC (
                          PE_USUARIO        IN     VARCHAR2,
                          PE_OBSERVACION    IN     VARCHAR2,
                          PS_ESTADO_SW      IN OUT BOOLEAN,
                          PS_MENSAJE_ERROR  IN OUT VARCHAR2
                          ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
PROCEDURE OBTENER_SW (
                  PS_REG_PARAMETROS IN OUT SCO_PARAMETROS%ROWTYPE
                  );
------------------------------------------------------------------------------------------------
FUNCTION DESACTIVAR_SW_PROCESOS_PAC (
                                     PS_MENSAJE_ERROR IN OUT VARCHAR2
                                     ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION OBTENER_ID_INSCRIPCION RETURN NUMBER;
------------------------------------------------------------------------------------------------
FUNCTION OBTENER_NOMBRE_ARCH_INSC (
                                   PE_CD_MEDIO_PAGO   IN     NUMBER,
                                   PE_TP_INSCRIPCION  IN     NUMBER,
                                   PE_ID_PROCESO      IN     VARCHAR2,
                                   PE_FECHA_ARCH_INSC IN     DATE,
                                   PS_NOMBRE_ARCHIVO  IN OUT VARCHAR2,
                                   PS_MENSAJE_ERROR   IN OUT VARCHAR2
                                   ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION GRABAR_SCOH_BIT_PROC_INSC_ARCH (
                                         PE_REG_SCOH_BIT_PROC_INSC_ARCH IN SCOH_BITACORA_PROC_INSC_ARCH%ROWTYPE,
                                         PS_MENSAJE_ERROR               IN OUT VARCHAR2
                                         ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION MODIFICAR_SCOH_BIT_PROC_INSC_A (
                                         PE_REG_SCOH_BIT_PROC_INSC_ARCH IN SCOH_BITACORA_PROC_INSC_ARCH%ROWTYPE,
                                         PS_MENSAJE_ERROR               IN OUT VARCHAR2
                                         ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- LUIS MORA DIAZ --
-- 08/05/2011 --
-- FUNCION TASA DE CAMBIO GENERAL PARA TRANSACCIONES PAC --
-- SE AGREGA LA TASA DE CAMBIO DE VENTA PARA PERU --
PROCEDURE PRC_TASA_CAMBIO_GENERAL (
                                   PE_FECHA_CAMBIO      IN     DATE,
                                   PE_CD_MONEDA         IN     VARCHAR2,
                                   PS_FECHA_EFECTIVA    IN OUT DATE,
                                   PS_MONTO_TASA_COMPRA IN OUT NUMBER,
                                   PS_MONTO_TASA_VENTA  IN OUT NUMBER
                                   );
------------------------------------------------------------------------------------------------
FUNCTION GRABAR_SCOH_BIT_PROC_PAC_ARCH (
                                        PE_REG_SCOH_BIT_PROC_PAC_ARCH IN SCOH_BITACORA_PROC_PAC_ARCH%ROWTYPE,
                                        PS_MENSAJE_ERROR              IN OUT VARCHAR2
                                        ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION MODIFICAR_SCOH_BIT_PROC_PAC_AR (
                                         PE_REG_SCOH_BIT_PROC_PAC_ARCH IN SCOH_BITACORA_PROC_PAC_ARCH%ROWTYPE,
                                         PS_MENSAJE_ERROR              IN OUT VARCHAR2
                                         ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION REGISTRO_CCTE (
                        PE_ID         IN     NUMBER,
                        PS_REG_CTACTE IN OUT SCO_CUENTA_CORRIENTE%ROWTYPE,
                        PS_TXT_ERR    IN OUT VARCHAR
                        ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION MONTO_ABONO_DIRECTO (
                              PE_USUARIO       IN     VARCHAR2,
                              PE_MONTO         IN     NUMBER,
                              PE_MONEDA        IN     VARCHAR2,
                              PS_MENSAJE_ERROR IN OUT VARCHAR2
                              ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION RETORNA_MEDIO_PAGO (
                             PE_PROPUESTA  IN     NUMBER,
                             PS_MEDIO_PAGO IN OUT NUMBER,
                             PS_TXT_ERR    IN OUT VARCHAR
                             ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
-- LUIS MORA DIAZ --
-- FECHA MODIFICACION 13-05-2010 --
-- SE ELIMINA FUNCION DE COBRANZA REGIONAL --
/*
FUNCTION GENERA_CTAS_CTES_PROPUESTA (
                                 PE_PROPUESTA      IN NUMBER,
                                 PE_MEDIO_PAGO     IN NUMBER,
                                 PS_MENSAJE_ERROR OUT VARCHAR2
                                 ) RETURN BOOLEAN;
*/
------------------------------------------------------------------------------------------------
-- LUIS MORA DIAZ --
-- 10-05-2011 --
-- SE ELIMINA FUNCION DE COBRANZA REGIONAL --
/*
FUNCTION ANULAR_MES_CERRADO (
                         PE_PROPUESTA     IN  NUMBER,
                         PS_MENSAJE_ERROR OUT VARCHAR2
                         ) RETURN BOOLEAN;
*/
------------------------------------------------------------------------------------------------
-- LUIS MORA DIAZ --
-- 10-05-2011 --
-- SE ELIMINA FUNCION DE LA COBRANZA REGIONAL --
/*
FUNCTION ANULAR_DESDE_CUADR_DIARIA_FILE (
                                     PS_ANULADOS_CORRECTOS IN OUT NUMBER,
                                     PS_ANULADOS_ERRONEOS  IN OUT NUMBER,
                                     PS_MENSAJE_ERROR      IN OUT VARCHAR2
                                     ) RETURN BOOLEAN;
*/
------------------------------------------------------------------------------------------------
-- LUIS MORA DIAZ --
-- 10-05-2011 --
-- SE ELIMINA FUNCION DE LA COBRANZA REGIONAL --
/*
FUNCTION ANULAR_DESDE_BIT_ANUL_FILE (
                                 PS_ANULADOS_CORRECTOS IN OUT NUMBER,
                                 PS_ANULADOS_ERRONEOS  IN OUT NUMBER,
                                 PS_MENSAJE_ERROR      IN OUT VARCHAR2
                                 ) RETURN BOOLEAN;
*/
------------------------------------------------------------------------------------------------
-- LUIS MORA DIAZ --
-- 10-05-2011 --
-- SE ELIMINA FUNCION DE LA COBRANZA REGIONAL --
/*
PROCEDURE GRABAR_SCO_BITACORA_ANULACION (
                                       PE_SCBA_ID_USUARIO_ESTADO IN VARCHAR2,
                                     PE_SCBA_FECHA_ESTADO       IN DATE,
                                     PE_SCBA_ESTADO               IN VARCHAR2,
                                     PE_SCBA_PROPUESTA           IN NUMBER,
                                     PE_SCBA_PUNTO_VENTA       IN NUMBER,
                                     PE_SCBA_NU_SOLICITUD       IN NUMBER
                                     );
*/
------------------------------------------------------------------------------------------------
PROCEDURE CORRECCION_FECHA_DIA_PAC;
------------------------------------------------------------------------------------------------
PROCEDURE RETORNA_CURSOR_SQL (
                              PE_SQL    IN     VARCHAR2,
                              PS_CURSOR IN OUT TDATOSSTATUS
                              );
------------------------------------------------------------------------------------------------
-- LUIS MORA DIAZ --
-- 17-05-2010 --
-- SE AGREGA FUNCION DE GENERACION DE CUOTA GRATIS --
FUNCTION GENERA_CUOTA_GRATIS (
                              PE_CD_PLAN       IN SCO_PRODUCTO_CUOTAGRATIS.SCOPCG_CD_PLAN%TYPE,
                              PE_PROPUESTA     IN SCO_CUENTA_CORRIENTE.CCTE_PROPUESTA%TYPE,
                              PS_MENSAJE_ERROR IN OUT VARCHAR2
                              ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION SIGUIENTE_FECHA_PAC (
                              PE_FECHA           IN     DATE,
                              PS_FECHA_SIGUIENTE IN OUT DATE,
                              PS_TXT_ERR         IN OUT VARCHAR2
                              ) RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION OBTENER_MONEDA_PROPUESTA (
                                   PE_PROPUESTA     IN     NUMBER,
                                   PS_REG_MONEDA    IN OUT CART_MONEDAS%ROWTYPE,
                                   PS_MENSAJE_ERROR IN OUT VARCHAR2
                                   ) RETURN BOOLEAN;
------------------------------------------------------------------------------------------------
FUNCTION VALIDA_FECHA_PAC (
                           PE_FECHA_PAC     IN     DATE,
                           PS_ES_FECHA_PAC  IN OUT BOOLEAN,
                           PS_MENSAJE_ERROR IN OUT VARCHAR2
                           ) RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION GRABAR_SCOH_INSCRIPCION (
                                  PE_SCOH_INSCRIPCION IN     SCOH_INSCRIPCION%ROWTYPE,
                                  PS_MENSAJE_ERROR    IN OUT VARCHAR2
                                  ) RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
FUNCTION MODIFICAR_SCO_INSCRIPCION (
                                    PE_SCO_INSCRIPCION IN     SCO_INSCRIPCION%ROWTYPE,
                                    PS_MENSAJE_ERROR   IN OUT VARCHAR2
                                    ) RETURN BOOLEAN;
---------------------------------------------------------------------------------------------------
-- LUIS MORA DIAZ --
-- 08/05/2011 --
-- FUNCION DE TRASPASO DE ENDOSOS DE COBRANZA DESDE SCO_ENDOSO_COBRANZA A SCO_CUENTA_CORRIENTE --
-- SE ELIMINA FUNCION PARA EL NUEVO MODELO DE COBRANZA PAC REGIONAL --
/*
FUNCTION TRASPASO_ENDOSO_COBRANZA (
                                PS_MENSAJE_ERROR         IN OUT VARCHAR2,
                               PS_REG_PROCESADOS        OUT    NUMBER,
                               PS_CARGO_DIRECTO         OUT    NUMBER,
                               PS_ABONO_DIRECTO         OUT    NUMBER,
                               PS_ABONO_DIRECTO_CAJA    OUT    NUMBER,
                               PS_ABONO_DIRECTO_CHEQUE  OUT    NUMBER,
                               PS_ABONO_DIRECTO_INTERES OUT    NUMBER
                               ) RETURN BOOLEAN;
*/
---------------------------------------------------------------------------------------------------
FUNCTION GRABAR_CART_CARGOS (
                             PE_REG_CARGOS    IN     CART_CARGOS%ROWTYPE,
                             PS_MENSAJE_ERROR IN OUT VARCHAR2
                             ) RETURN BOOLEAN;
-----------------------------------------------------------------------
-- SCIFUENTES 12-05-2011 --
FUNCTION FN_CALCULA_DIA_PAC (
                             PE_MEDIO_PAGO  IN NUMBER,
                             PE_DIA         IN NUMBER
                             ) RETURN NUMBER;
-----------------------------------------------------------------------
-- SCIFUENTES  -- 13-05-2011  --
-- RETORNA FECHA PAC PARA RECTOR --
FUNCTION FN_RETORNA_FECHA_PAC_RECTOR (
                                      PE_NU_PROPUESTA  IN     NUMBER,
                                      PS_FECHA_PAC     IN OUT DATE,
                                      PS_MENSAJE_ERROR IN OUT VARCHAR2
                                      ) RETURN BOOLEAN;
-----------------------------------------------------------------------
--IC--20110304-SGALLEGOS-[ER-RECTOR-PROYCOB_EST_REG] - SE CREA FUNCION PARA HOMOLOGAR ESTADO DE CTA CTE CON RECIBO --FC
FUNCTION FN_HOMOLOGA_ST_CCTE_RECIBOS (
                                      PE_ST_CCTE IN NUMBER
                                      ) RETURN VARCHAR2;
-----------------------------------------------------------------------
--IC--20110303-SGALLEGOS-[ER-RECTOR-PROYCOB_EST_REG] : SE CREA FUNCION DE ACTUALIZACION DE FECHAS DE RECIBO   --FC
FUNCTION FN_ACTUALIZA_FECHAS_RECIBO (
                                         PE_CCTE_RECIBO             IN NUMBER,
                                     PE_NUEVA_FECHA_PROCESO_PAC IN DATE
                                     ) RETURN BOOLEAN;

-----------------------------------------------------------------------
--IC--20110303-SGALLEGOS-[ER-RECTOR-PROYCOB_EST_REG] : SE CREA FUNCION DE ACTUALIZACION ESTADO RECIBO   --FC
FUNCTION FN_ACTUALIZA_ESTADO_RECIBO (
                                     PE_CCTE_RECIBO    IN NUMBER,
                                     PE_CCTE_STATUS    IN VARCHAR2,
                                     PS_MENSAJE_ERROR OUT VARCHAR2
                                     ) RETURN BOOLEAN;
/*
-----------------------------------------------------------------------
--IC--20110304-SGALLEGOS-[ER-RECTOR-PROYCOB_EST_REG] : SE CREA FUNCION PARA SABER SI HAY UN PAC ACTIVO PARA UN ID DE CTA CTE   --FC
FUNCTION FN_EXISTE_PAC_ACTIVO (PE_CCTE_ID        IN NUMBER ,
                             PS_MENSAJE_ERROR OUT VARCHAR2) RETURN BOOLEAN;

*/
-----------------------------------------------------------------------
-- SCIFUENTES  -- 11-05-2011  --
-- RETORNA TIPO DE ARCHIVO ASOCIADO A UN MOVIMIENTO DE LA CUENTA CORRIENTE --
FUNCTION FN_RETORNA_TP_ARCHIVO_CCTE_ID (
                                        PE_CCTE_ID       IN     NUMBER,
                                        PS_TP_ARCHIVO    IN OUT NUMBER,
                                        PS_MENSAJE_ERROR IN OUT VARCHAR2
                                        ) RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION FN_ESTADO_SEMAFORO_CCTE_ID (
                                     PE_CCTE_ID       IN     NUMBER,
                                     PS_ESTADO        IN OUT VARCHAR2,
                                     PS_MENSAJE_ERROR IN OUT VARCHAR2
                                     )  RETURN BOOLEAN;
-----------------------------------------------------------------------
-- SCIFUENTES  -- 09-05-2011  --
-- RETORNA CURSOR CON CUOTAS PENDIENTES PARA ENDOSO CAMBIO DE PRIMA --
PROCEDURE PRC_END_RETORNA_CUOTAS_CMB_PRI (
                                          PE_PROPUESTA    IN     NUMBER,
                                          PS_CUR_CTA_CTE  IN OUT TCURSOR
                                          );
-----------------------------------------------------------------------
-- SCIFUENTES  -- 09-05-2011  --
-- RETORNA CURSOR CON PROPUESTAS PENDIENTES Y REZAGAZAS PARA ENDOSO CAMBIO DE CUENTA --
PROCEDURE PRC_END_RETORNA_CUOTAS_CMB_CTA (
                                          PE_PROPUESTA    IN     NUMBER,
                                          PS_CUR_CTA_CTE  IN OUT TCURSOR
                                          );
-----------------------------------------------------------------------
-- SCIFUENTES  -- 09-05-2011  --
-- RETORNA CURSOR CON PROPUESTAS PENDIENTES Y REZAGAZAS PARA ENDOSO DE ANULACI�M  --
PROCEDURE PRC_END_RETORNA_CUOTAS_ANUL (
                                       PE_PROPUESTA    IN     NUMBER ,
                                       PS_CUR_CTA_CTE  IN OUT TCURSOR
                                       );
-----------------------------------------------------------------------
-- SCIFUENTES  -- 09-05-2011  --
-- RETORNA CURSOR CON PROPUESTAS PENDIENTES Y REZAGAZAS PARA ENDOSO DE REHABILITACION  (REACTIVACION) --
PROCEDURE PRC_END_RETORNA_CUOTAS_REHAB (
                                        PE_PROPUESTA   IN     NUMBER ,
                                        PS_CUR_CTA_CTE IN OUT TCURSOR
                                        );
-----------------------------------------------------------------------
-- LUIS MORA DIAZ --
-- 08/05/2011 --
-- SE OBTIENE REGISTRO DE LA CUADRATURA DIARIA PARA VALIDAR EN LOS ENDOSOS --
FUNCTION FN_VALIDA_CUADRATURA (
                               PE_PROPUESTA      IN     NUMBER,
                               PS_REG_CUADRATURA IN OUT SCO_CUADRATURA_DIARIA%ROWTYPE,
                               PS_MENSAJE_ERROR  IN OUT VARCHAR2
                               ) RETURN BOOLEAN;
-----------------------------------------------------------------------
-- LUIS MORA DIAZ --
-- 08/05/2011 --
-- SE OBTIENE REGISTRO DE LA INSCRIPCION PARA VALIDAR EN LOS ENDOSOS --
FUNCTION FN_VALIDA_INSCRIPCION (
                                PE_PROPUESTA       IN     NUMBER,
                                PS_REG_INSCRIPCION IN OUT SCO_INSCRIPCION%ROWTYPE,
                                PS_MENSAJE_ERROR   IN OUT VARCHAR2
                                ) RETURN BOOLEAN;
-----------------------------------------------------------------------
-- SCIFUENTES -- 09-05-2011 --
-- FUNCI�N QUE VALIDA PROPUESTA EN PAC PARA ENDOSO CAMBIO DE PRIMA --
FUNCTION FN_END_VALIDA_CAMBIO_PRIMA (
                                     PE_NU_PROPUESTA   IN     CART_CERTIFICADOS.CACE_NU_PROPUESTA%TYPE,
                                     PS_CUOTAS_VALIDAS IN OUT NUMBER,
                                     PS_MENSAJE_ERROR  IN OUT VARCHAR2
                                     ) RETURN BOOLEAN;
-----------------------------------------------------------------------
-- SCIFUENTES -- 09-05-2011 --
-- FUNCI�N QUE VALIDA PROPUESTA EN PAC PARA ENDOSO CAMBIO DE PRIMA --
FUNCTION FN_END_VALIDA_CAMBIO_CUENTA (
                                      PE_NU_PROPUESTA   IN     CART_CERTIFICADOS.CACE_NU_PROPUESTA%TYPE,
                                      PS_CUOTAS_VALIDAS IN OUT NUMBER,
                                      PS_MENSAJE_ERROR  IN OUT VARCHAR2
                                      ) RETURN BOOLEAN;
-----------------------------------------------------------------------
--IC--20110304-SGALLEGOS-[ER-RECTOR-PROYCOB_EST_REG] : SE CREA FUNCION PARA ACTUALIZAR CAMPOS PARA UN ID DE CTA CTE   --FC
/*
FUNCTION FN_ACTUALIZA_CAMPOS_CTA_CTE (PE_CCTE_ID              IN NUMBER ,
                                    PE_CCTE_MONTO_MONEDA    IN NUMBER,
                                    PE_CCTE_FECHA_ENVIO_PAC IN DATE,
                                    PE_CCTE_RECIBO          IN NUMBER,
                                    PE_CCTE_NU_ENDOSO       IN NUMBER,
                                    PS_ERROR               OUT VARCHAR2 ) RETURN BOOLEAN;

-----------------------------------------------------------------------
--IC--20110304-SGALLEGOS-[ER-RECTOR-PROYCOB_EST_REG] : SE CREA FUNCION PARA ACTUALIZA LA PRIMA BRUTA SEGUN PROPUESTA   --FC
FUNCTION FN_ACT_PRIMA_BRUTA  (PE_PROPUESTA             IN NUMBER ,
                            PE_PRIMA_MENSUAL         IN NUMBER   ,
                            PS_ERROR              OUT VARCHAR2 ) RETURN BOOLEAN ;


-----------------------------------------------------------------------
--IC--20110310-SGALLEGOS-[ER-RECTOR-PROYCOB_EST_REG]SE MODIFICA FUNCION MIGRADA A PERU.CAMBIA_ESTADO_CUOTA A =>CAMBIA_DATOS_CUOTA_PAC , AGREGANDO PARAMETROS FALTANTES (RECIBO , ENDOSO)
FUNCTION CAMBIA_DATOS_CUOTA_PAC (
                               PE_ID       IN NUMBER,
                               PE_STATUS   IN NUMBER,
                               PE_RECIBO   IN NUMBER,
                               PE_ENDOSO   IN NUMBER,
                               PS_TXT_ERR OUT VARCHAR2
                               ) RETURN BOOLEAN;


*/
FUNCTION FN_END_VALIDA_ANULACION (
                                  PE_NU_PROPUESTA   IN     CART_CERTIFICADOS.CACE_NU_PROPUESTA%TYPE,
                                  PS_CUOTAS_VALIDAS IN OUT  NUMBER,
                                  PS_MENSAJE_ERROR  IN OUT  VARCHAR2
                                  ) RETURN BOOLEAN;

FUNCTION FN_END_VALIDA_REHAB (
                              PE_NU_PROPUESTA   IN     CART_CERTIFICADOS.CACE_NU_PROPUESTA%TYPE,
                              PS_CUOTAS_VALIDAS IN OUT  NUMBER,
                              PS_MENSAJE_ERROR  IN OUT  VARCHAR2
                              ) RETURN BOOLEAN;

FUNCTION FN_END_VALIDA_RENOVACION (
                                   PE_NU_PROPUESTA   IN     CART_CERTIFICADOS.CACE_NU_PROPUESTA%TYPE,
                                   PS_MENSAJE_ERROR  IN OUT  VARCHAR2
                                   ) RETURN BOOLEAN;

FUNCTION FN_END_VALIDA_DESIST_CARGOS (
                                      PE_NU_PROPUESTA   IN     CART_CERTIFICADOS.CACE_NU_PROPUESTA%TYPE,
                                       PS_CUOTAS_VALIDAS IN OUT  NUMBER,
                                       PS_MENSAJE_ERROR  IN OUT  VARCHAR2
                                      ) RETURN BOOLEAN;

FUNCTION FN_END_VALIDA_CAMBIO_MPAGO (
                                     PE_NU_PROPUESTA   IN     CART_CERTIFICADOS.CACE_NU_PROPUESTA%TYPE,
                                     PS_CUOTAS_VALIDAS IN OUT  NUMBER,
                                     PS_MENSAJE_ERROR  IN OUT  VARCHAR2
                                     ) RETURN BOOLEAN;
--------------------------------------------------------------------------------
--IC--20110507-SCIFUENTES --[ER-RECTOR-PROYCOB_EST_REG] : SE CREA FUNCION PARA ACTUALIZA LA PRIMA BRUTA SEGUN PROPUESTA  =>FN_ACT_PRIMA_BRUTA --FC
FUNCTION FN_END_CAMBIO_PRIMA_CUADRATURA (
                                         PE_PROPUESTA     IN     NUMBER,
                                         PE_PRIMA_MENSUAL IN     NUMBER,
                                         PS_MENSAJE_ERROR IN OUT VARCHAR2
                                         ) RETURN BOOLEAN;
--------------------------------------------------------------------------------
--IC--20110507-SCIFUENTES-[ER-RECTOR-PROYCOB_EST_REG] : SE CREA FUNCION PARA ACTUALIZA EL ESTADO PARA LA PROPUESTA --FC
FUNCTION FN_END_ANULACION_CUADRATURA  (
                                       PE_PROPUESTA     IN     NUMBER,
                                       PS_MENSAJE_ERROR IN OUT VARCHAR2
                                       ) RETURN BOOLEAN;
--------------------------------------------------------------------------------
--IC--20110507-SCIFUENTES-[ER-RECTOR-PROYCOB_EST_REG] : SE CREA FUNCION PARA REHABILITAR LA PROPUESTA EN CUADRATURA DIARIA --FC
FUNCTION FN_END_REACTIVACION_CUADRATURA (
                                         PE_PROPUESTA     IN     NUMBER,
                                         PS_MENSAJE_ERROR IN OUT VARCHAR2
                                         ) RETURN BOOLEAN;

/*
-----------------------------------------------------------------------
FUNCTION ACTUALIZA_ST_CUADRATURA_DIARIA   (PE_PROPUESTA     IN NUMBER,
                                         PE_NUEVO_ESTADO  IN NUMBER,
                                         PS_ERROR         OUT VARCHAR2 )RETURN BOOLEAN;

-----------------------------------------------------------------------
--IC--20110310-SGALLEGOS-[ER-RECTOR-PROYCOB_EST_REG] : SE CREA FUNCION DE REACTIVACION PARA ACTUALIZAR LA SCO_CUADRATURA_DIARIA (DE 3 CAMBIA A 1),
FUNCTION FN_REACTIVACION_PAC (PE_PROPUESTA     IN NUMBER,
                            PS_ERROR        OUT VARCHAR2)RETURN BOOLEAN;

-----------------------------------------------------------------------
--IC--20110304-SGALLEGOS-[ER-RECTOR-PROYCOB_EST_REG] : SE CREA FUNCION QUE RETORNA TODAS LAS CUOTAS ANULADAS (ESTADO1) --FC
PROCEDURE PROC_RETORNA_CUOTAS_ANULADAS (PE_PROPUESTA IN NUMBER,
                                      PS_CUR_CTA_CTE  OUT TCURSOR); -- SCO_CUENTA_CORRIENTE%ROWTYPE);

*/
-----------------------------------------------------------------------
--IC--20110506-SCIFUENTES-[ER-RECTOR-PROYCOB_EST_REG] : SE CREA FUNCION PARA CAMBIO DE PRIMA EN CUENTA CORRIENTE --FC
FUNCTION FN_END_CAMBIO_PRIMA (
                              PE_CCTE_PROPUESTA       IN     NUMBER,
                              PE_CCTE_RECIBO_ANT      IN     NUMBER,
                              PE_CCTE_MONEDA          IN     VARCHAR2,
                              PE_CCTE_MONTO_MONEDA    IN     NUMBER,
                              PE_CCTE_PRIMA_AFECTA    IN     NUMBER,
                              PE_CCTE_PRIMA_EXENTA    IN     NUMBER,
                              PE_CCTE_IVA             IN     NUMBER,
                              PE_CCTE_FECHA_ENVIO_PAC IN     DATE,
                              PE_CCTE_RECIBO          IN     NUMBER,
                              PE_CCTE_NU_ENDOSO       IN     NUMBER,
                              PS_MENSAJE_ERROR        IN OUT VARCHAR2
                              ) RETURN BOOLEAN;
-----------------------------------------------------------------
--IC--20110507-SCIFUENTES-[ER-RECTOR-PROYCOB_EST_REG] : SE CREA FUNCION PARA ELIMINAR CUENTAS EN CUENTA CORRIENTE --FC
FUNCTION FN_END_ANULACION (
                           PE_CCTE_ID       IN     NUMBER,
                           PS_MENSAJE_ERROR IN OUT VARCHAR2
                           ) RETURN BOOLEAN;
-----------------------------------------------------------------
--IC--20110507-SCIFUENTES-[ER-RECTOR-PROYCOB_EST_REG] : SE CREA FUNCION PARA REHBILITAR CUOTAS EN CUENTA CORRIENTE --FC
FUNCTION FN_END_REHABILITACION (
                                PE_CCTE_ID            IN     NUMBER,
                                PE_CCTE_FECHA_ENV_PAC IN     DATE,
                                PS_MENSAJE_ERROR      IN OUT VARCHAR2
                                ) RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION VALIDA_ULTIMO_ENDOSO (
                               PE_PROPUESTA IN     CART_CERTIFICADOS.CACE_NU_PROPUESTA%TYPE,
                               PE_MSJ       IN OUT VARCHAR2
                               ) RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION GRABAR_NORMALIZACION (
                               PE_PROPUESTA         IN      NUMBER,   --NRO PROPUESTA
                               PE_TP_MOVTO          IN      NUMBER,   --VALORES 2 CARGOS-3 ABONO
                               PE_MONEDA            IN      VARCHAR2, --TIPO MONEDA
                               PE_MONTO             IN      NUMBER,   --MONTO
                               PE_FECHA_ENVIO_PAC   IN      DATE,     --FECHA ENVIO PAC--
                               PE_CAOP_CD_OPERACION IN      NUMBER,   --CODIGO OPERACION DIARIA --
                               PE_CAOP_CD_SUB_OPER  IN      NUMBER,   --CODIGO SUB OPERACION DIARIA --
                               PE_CNMO_CD_MOTIVO    IN      VARCHAR2, --CODIGO DE MOTIVO DE NORMALIZACION --
                               PE_CCTE_RECIBO       IN      NUMBER ,  --NUMBER--
                               PE_CCTE_NU_ENDOSO    IN      NUMBER,   --NUMBER--
                               PS_MENSAJE_ERROR     IN OUT  VARCHAR2
                               ) RETURN BOOLEAN;
-----------------------------------------------------------------------
PROCEDURE CERTIFICACION_RECIBO_CUOTA_PAC (
                                          PE_PROPUESTA IN CART_CERTIFICADOS.CACE_NU_PROPUESTA%TYPE
                                          );
-----------------------------------------------------------------------
FUNCTION CAMBIO_MEDIO_PAGO (
                            PE_NU_PROPUESTA       IN     NUMBER,
                            PE_NU_ENDOSO          IN     NUMBER,
                            PE_CD_MEDIO_PAGO_ORI  IN     NUMBER,
                            PE_CD_MEDIO_PAGO_DES  IN     NUMBER,
                            PE_CAFR_CD_FRAGMENT   IN     NUMBER,
                            PE_NU_RUT              IN     VARCHAR2,
                            PE_NU_CUENTA          IN     VARCHAR2,
                            PE_NU_CUOTAS_PACTADAS IN     NUMBER,
                            PS_MENSAJE_ERROR      IN OUT VARCHAR2
                            ) RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION FN_CALCULA_DIA_PAC (
                             PE_PROPUESTA          IN     NUMBER,
                             PS_FECHA_ENVIO_PAC    IN OUT DATE, 
                             PS_MENSAJE_ERROR      IN OUT VARCHAR2
                             ) RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION FN_CALCULA_SIGUIENTE_FECHA_PAC (
                                         PE_CD_MEDIO_PAGO IN NUMBER,
                                         PE_FECHA_PAC     IN DATE,
                                         PS_SIG_FECHA_PAC IN OUT DATE,
                                         PS_MENSAJE_ERROR IN OUT VARCHAR2
                                         ) RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION FN_PRIMER_DIA_PAC(
   PE_MEDIO_PAGO IN     NUMBER
  ,PS_DIA_PAC    IN OUT NUMBER
  ,PS_MSG_ERROR  IN OUT VARCHAR2
) RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION FN_ULTIMO_DIA_PAC(
   PE_MEDIO_PAGO IN     NUMBER
  ,PS_DIA_PAC    IN OUT NUMBER
  ,PS_MSG_ERROR  IN OUT VARCHAR2
) RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION FN_VALIDA_DIA_PAC(
   PE_MEDIO_PAGO IN NUMBER
  ,PE_DIA_PAC    IN NUMBER
) RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION FN_DIA_PAGO(
   PE_SODP_MEDIO_PAGO IN     SCO_DIA_PAGO.SODP_MEDIO_PAGO%TYPE  
  ,PE_SODP_DIA_PAC    IN     SCO_DIA_PAGO.SODP_DIA_PAC%TYPE
  ,PS_SODP_DIA_PAGO   IN OUT SCO_DIA_PAGO.SODP_DIA_PAGO%TYPE
  ,PS_MENSAJE_ERROR   IN OUT VARCHAR2
) RETURN BOOLEAN;
-----------------------------------------------------------------------
--JORTEGA  SE COMENTA FUNCION PARA PASAR MEJORA DE DESFASE DE COBRANZA
--29-05-2012 
FUNCTION FN_IMPLICITO(
   PE_SCDEI_CD_PLAN           IN     SCO_DEF_ENVIO_INSCRIPCION.SCDEI_CD_PLAN%TYPE,
   PE_SCDEI_CD_MEDIO_PAGO     IN     SCO_DEF_ENVIO_INSCRIPCION.SCDEI_CD_MEDIO_PAGO%TYPE, 
   PE_SCDEI_TP_REGISTRO       IN     SCO_DEF_ENVIO_INSCRIPCION.SCDEI_TP_REGISTRO%TYPE,
   MODO_INSCRIPCION_IMPLICITO IN OUT BOOLEAN,
   PS_MENSAJE_ERROR           IN OUT VARCHAR2
) RETURN BOOLEAN;
-----------------------------------------------------------------------
-- SCIFUENTES -- 26-04-2012 --
-- SE CREA FUNCION AGREGA CUOTA CON PARAMETRO DE MONEDA --
/*FUNCTION FN_AGREGAR_CUOTA_PAC (
                               PE_PROPUESTA         IN      NUMBER,   --NRO PROPUESTA 
                               PE_CUOTA             IN      NUMBER,   --NRO CUOTA  
                               PE_PERIODO           IN      NUMBER,   --NRO PERIODO   
                               --PE_CCTE_STATUS       IN      NUMBER;   --ESTADO DE LA CUOTA -- PENDIENTE --
                               PE_FECHA_ENVIO_PAC   IN      DATE,     --FECHA ENVIO PAC--
                               PE_FECHA_DESDE       IN      DATE,     --FECHA INICIO VIGENCIA CUOTA  
                               PE_FECHA_HASTA       IN      DATE,     --FECHA FIN VIGENCIA CUOTA    
                               PE_TOTAL_CUOTAS      IN      NUMBER,   --NRO FRAGMENT 
                               PE_MONEDA            IN      VARCHAR2, --TIPO MONEDA 
                               PE_MONTO_MONEDA      IN      NUMBER,   --MONTO MONEDA  
                               PE_CCTE_PRIMA_AFECTA IN      NUMBER,   --MONTO PRIMA AFECTA   
                               PE_CCTE_PRIMA_EXENTA IN      NUMBER,   --MONTO PRIMA EXENTA   
                               PE_CCTE_IVA          IN      NUMBER,   --MONTO IVA 
                               PE_CCTE_RECIBO       IN      NUMBER ,  --NRO RECIBO RECTOR 
                               PE_CCTE_NU_ENDOSO    IN      NUMBER,   --NRO ENDOSO RECTOR 
                               PE_CAOP_CD_OPERACION IN      NUMBER,   --CODIGO OPERACION DIARIA --
                               PE_CAOP_CD_SUB_OPER  IN      NUMBER,   --CODIGO SUB OPERACION DIARIA --
                               PE_CD_USUARIO        IN      VARCHAR2,  --CODIGO DEL USUARIO  
                               PS_MENSAJE_ERROR     IN OUT  VARCHAR2
                               ) RETURN BOOLEAN;*/
-----------------------------------------------------------------------
FUNCTION OBTENER_ID_BIT_CAMB_MEDIO_PAGO RETURN NUMBER;
-----------------------------------------------------------------------
FUNCTION GRABAR_SCOH_BIT_CAMB_MED_PAGO (
                                        PE_SCOH_BIT_CAMB_MEDIO_PAGO IN     SCOH_BITACORA_CAMB_MEDIO_PAGO%ROWTYPE,
                                        PS_MENSAJE_ERROR            IN OUT VARCHAR2
                                        ) RETURN BOOLEAN;
----------------------------------------------------------------------                                        
FUNCTION FN_END_GRABAR_BIT_CAMB_MP (
                                    PE_NU_PROPUESTA       IN     NUMBER,
                                    PE_NU_ENDOSO          IN     NUMBER,
                                    PE_CD_MEDIO_PAGO_ORI  IN     NUMBER,
                                    PE_CD_MEDIO_PAGO_DES  IN     NUMBER,
                                    PE_NU_CUENTA_ANT      IN     NUMBER,
                                    PE_NU_CUENTA_NEW      IN     NUMBER,
                                    PE_CAFR_CD_FRAGMENT   IN     NUMBER,
                                    PE_NU_CUOTAS_PACTADAS IN     NUMBER,
                                    PE_CD_PUNTO_VENTA     IN     NUMBER,
                                    PS_MENSAJE_ERROR      IN OUT VARCHAR2
                                    ) RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION FN_END_GRABAR_BIT_CAMB_CUENTA (
                                        PE_NU_PROPUESTA       IN     NUMBER,
                                        PE_NU_ENDOSO          IN     NUMBER,
                                        PE_CD_MEDIO_PAGO_ORI  IN     NUMBER,
                                        PE_CD_MEDIO_PAGO_DES  IN     NUMBER,
                                        PE_NU_CUENTA_ANT      IN     NUMBER,
                                        PE_NU_CUENTA_NEW      IN     NUMBER,
                                        PE_CAFR_CD_FRAGMENT   IN     NUMBER,
                                        PE_NU_CUOTAS_PACTADAS IN     NUMBER,
                                        PE_CD_PUNTO_VENTA     IN     NUMBER,
                                        PS_MENSAJE_ERROR      IN OUT VARCHAR2
                                        ) RETURN BOOLEAN;
-----------------------------------------------------------------------
FUNCTION FN_END_CAMBIO_MEDIO_PAGO (
                                   PE_NU_PROPUESTA       IN     NUMBER,
                                   PE_NU_ENDOSO          IN     NUMBER,
                                   PE_CD_MEDIO_PAGO_ORI  IN     NUMBER,
                                   PE_CD_MEDIO_PAGO_DES  IN     NUMBER,
                                   PE_TP_DOCUMENTO       IN     CART_CLIENTES.CACN_TP_DOCUMENTO%TYPE,
                                   PE_NU_DOCUMENTO       IN     CART_CLIENTES.CACN_NU_DOCUMENTO%TYPE,
                                   PE_NU_CUENTA_ANT      IN     VARCHAR2,
                                   PE_NU_CUENTA_NEW      IN     VARCHAR2,
                                   PE_CAFR_CD_FRAGMENT   IN     NUMBER,
                                   PE_NU_CUOTAS_PACTADAS IN     NUMBER,
                                   PE_CD_PUNTO_VENTA     IN     NUMBER,
                                   PS_MENSAJE_ERROR      IN OUT VARCHAR2
                                   ) RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------
-- LMD --
-- 24/08/2012 --
-- SE AGREGA FUNCION QUE PERMITE HOMOLOGAR LOS ESTADOS DE RECTOR V/S PAC --
FUNCTION FN_HOMOLOGA_ST_CART_RECIBOS (
                                      PE_CARE_ST_RECIBO IN     CART_RECIBOS.CARE_ST_RECIBO%TYPE,
                                      PS_CCTE_STATUS    IN OUT SCO_CUENTA_CORRIENTE.CCTE_STATUS%TYPE,
                                      PS_MENSAJE_ERROR  IN OUT VARCHAR2
                                      ) RETURN BOOLEAN;
--------------------------------------------------------------------------------------------------                                      
PROCEDURE AVISO_STATUS ( V_ID      IN NUMBER,
                         M_STATUS OUT VARCHAR2 );
                         
--------------------------------------------------------------------------------------------------
FUNCTION FN_OBTENER_NRO_CUENTA( p_cNumeroTarjeta IN VARCHAR2 ) RETURN VARCHAR2;


END; 
/
